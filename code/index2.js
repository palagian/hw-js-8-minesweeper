/* Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю.
Если в данной ячейке находится мина, игрок проиграл. В таком случае показать все остальные мины на поле. 
Другие действия стают недоступны, можно только начать новую игру.
Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все соседние ячейки с цифрами.
Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
После первого хода над полем должна появляться кнопка "Начать игру заново", которая будет обнулять предыдущий 
результат прохождения и заново инициализировать поле.
Над полем должно показываться количество расставленных флажков, и общее количество мин (например `7 / 10`). */

window.onload = () => {

    const WIDTH = 8;
    const HEIGHT = 8;
    const BOMBS_COUNT = 10;

    const field = document.querySelector(".field");

    let flagsCount = 0;

    // создаем кнопку "Новая игра"
    btnNewGame = document.createElement("button");
    btnNewGame.innerText = "NEW GAME";
    btnNewGame.classList.add("btnNewGame");
    field.before(btnNewGame);

    // функция для кнопки NEW GAME - запускает новую игру
    const newGame = () => {
        btnNewGame.addEventListener("click", (e) => {
            const counter = document.querySelector(".counter");
            counter.innerHTML = "FLAGS: 0 / BOMBS: 10";
            let result = document.querySelector(".result");
            result.innerHTML = "";
            flagsCount = 0;
            field.removeEventListener("contextmenu", () => { });
            document.location = document.location.hostname;
        })
    }

    function startGame(width = WIDTH, height = HEIGHT, bombs_count = BOMBS_COUNT) {

        // рисуем поле для игры
        const cellsCount = width * height;
        field.innerHTML = "<button></button>".repeat(cellsCount);

        let result = document.querySelector(".result");
        let closedCells = WIDTH * HEIGHT;

        const allCells = [...field.children];
        const bombCells = [];
        let bombCell;

        // разбрасываем мины случайным образом
        const randomBombs = () => {
            let i = 0;
            while (i < bombs_count) {
                bombCell = Math.round(Math.random() * cellsCount);

                if (bombCells.includes(bombCell)) {
                    continue;
                } else {
                    bombCells.push(bombCell);
                    i++;
                }
            }
            console.log(bombCells);
        };

        // находим соседние ячейки
        const neighborBombs = (ci) => {

            nghbrs = getNeighbors(ci);
            count = getBombsCountAround(nghbrs);

            if (count !== 0) { // если вокруг есть бомбы, выводим их количество
                allCells[ci].innerText = count;
                allCells[ci].disabled = true;
            } else { // если бомб нет, перебираем соседей для соседних индексов, пока не наткнемся на счет бомб
                nghbrs.forEach(ni => {
                    if (!allCells[ni].disabled) {
                        allCells[ni].disabled = true;
                        if (allCells[ni].classList.contains("flag")) {
                            allCells[ni].classList.remove("flag");
                        }
                        neighborBombs(ni);
                    }
                });
            }
        }

        // считаем бомбы в соседях
        const getBombsCountAround = (elements = []) => {
            count = 0;
            elements.forEach(ni => {
                if (bombCells.includes(ni)) {
                    count++;
                }
            });
            return count;
        }

        // создаем массив с соседними ячейками
        const getNeighbors = ci => {
            nghbrs = [];

            topLeftI = ci - 1 - WIDTH;
            topCenterI = ci - WIDTH;
            topRightI = ci + 1 - WIDTH;
            leftI = ci - 1;
            rightI = ci + 1;
            bottomLeftI = ci - 1 + WIDTH;
            bottomCenterI = ci + WIDTH;
            bottomRightI = ci + 1 + WIDTH;

            if (ci >= WIDTH) { // добавляем в соседей верхний ряд
                if (ci % WIDTH !== 0 && !allCells[topLeftI].disabled)
                    nghbrs.push(topLeftI);
                if (!allCells[topCenterI].disabled)
                    nghbrs.push(topCenterI);
                if (ci % WIDTH !== WIDTH - 1 && !allCells[topRightI].disabled)
                    nghbrs.push(topRightI);
            }

            if (ci % WIDTH !== 0 && !allCells[leftI].disabled) // добавляем в соседей ячейки слева и справа
                nghbrs.push(leftI);
            if (ci % WIDTH !== WIDTH - 1 && !allCells[rightI].disabled)
                nghbrs.push(rightI);
            if (ci < WIDTH * HEIGHT - WIDTH) { // добавляем в соседей нижний ряд
                if (ci % WIDTH !== 0 && !allCells[bottomLeftI].disabled)
                    nghbrs.push(bottomLeftI);
                if (!allCells[bottomCenterI].disabled)
                    nghbrs.push(bottomCenterI);
                if (ci % WIDTH !== WIDTH - 1 && !allCells[bottomRightI].disabled)
                    nghbrs.push(bottomRightI);
            }
            return nghbrs;
        }

        // создаем событие на клик левой клавишей мыши (ЛКМ)
        field.addEventListener("click", (e) => {
            let index = allCells.indexOf(e.target);

            if (index != -1) {
                // функция для окончания игры, если попал на мину
                if (bombCells.includes(index)) {
                    stopGame();
                    flagsCount = 0;
                } else {
                    neighborBombs(index);
                    allCells[index].disabled = true;
                };
            };
            closedCells = WIDTH * HEIGHT;
            victory();
        })

        // создаем функцию конца игры, если попал на мину
        const stopGame = () => {
            allCells.forEach(el => {
                index = allCells.indexOf(el);
                allCells[index].disabled = true;
                allCells[index].classList.remove("flag");
                flagsCount = 0;
                if (bombCells.includes(index)) {
                    allCells[index].innerHTML = "<img src='./img/mine.png'>";
                }
            })
            result.innerHTML = "YOU LOOSE! TRY AGAIN!"
        }

        // функция для победы
        const victory = () => {

            allCells.forEach(el => {
                if (el.disabled) {
                    closedCells--;
                }
                return closedCells;
            });
            if (closedCells === BOMBS_COUNT) {
                result.innerHTML = "YOU WIN! CONGRATULATIONS!";
            }
        }

        // создаем событие на клик правой клавишей мыши (ПКМ)
        field.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            const element = e.target;
            let index = allCells.indexOf(element);

            // если ячейка открыта, пустая или находится вне поля - ничего не делаем
            if (element.disabled || element.tagName.toLowerCase() !== "button") {
                return;
            }

            if (element.classList.contains("flag")) {
                element.classList.remove("flag"); // если флаг уже стоит - убираем 
                flagsCount--;

            } else { // в остальных случаях ставим флажок
                element.classList.add("flag");
                flagsCount++;
            }

            // создаем счетчик флагов и мин          
            const counter = document.querySelector(".counter");
            counter.innerHTML = `FLAGS: ${flagsCount} / BOMBS: ${BOMBS_COUNT}`;
            e.stopPropagation();
        })

        randomBombs();
        newGame();
    }
    startGame();
}
